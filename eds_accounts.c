/* gcc `pkg-config --cflags --libs libedataserver-1.2` test.c -g -O0 -o test && ./test */

#include <stdio.h>
#include <libedataserver/libedataserver.h>

int main (int argc, char *argv[])
{
	ESourceRegistry *registry;
	GHashTable *goas;
	GHashTableIter iter;
	gpointer key, value;
	GList *sources, *link;
	GError *error = NULL;

	registry = e_source_registry_new_sync (NULL, &error);
	if (error) {
		g_warning ("Failed to create registry: %s", error->message);
		g_clear_error (&error);
		return 1;
	}

	if (!registry) {
		g_warning ("Registry is NULL, but no error given");
		return 2;
	}

	goas = g_hash_table_new (g_str_hash, g_str_equal);
	sources = e_source_registry_list_sources (registry, NULL);

	for (link = sources; link; link = g_list_next (link)) {
		ESource *source = link->data;

		if (e_source_has_extension (source, E_SOURCE_EXTENSION_GOA))
			g_hash_table_insert (goas, (gpointer) e_source_get_uid (source), source);
	}

	printf ("Found %d GOA enabled accounts\n", g_hash_table_size (goas));
	g_hash_table_iter_init (&iter, goas);
	while (g_hash_table_iter_next (&iter, &key, &value)) {
		ESource *source = value;
		ESourceCollection *collection;

		collection = e_source_get_extension (source, E_SOURCE_EXTENSION_COLLECTION);

		printf ("Account '%s'\n", e_source_get_display_name (source));
		printf ("   Enabled: %s\n", e_source_get_enabled (source) ? "true" : "false");
		printf ("   Backend: %s\n", e_source_backend_get_backend_name (E_SOURCE_BACKEND (collection)));
		printf ("   Calendar: %senabled\n", e_source_collection_get_calendar_enabled (collection) ? "" : "dis");
		printf ("   Contacts: %senabled\n", e_source_collection_get_contacts_enabled (collection) ? "" : "dis");
		printf ("   Mail: %senabled\n", e_source_collection_get_mail_enabled (collection) ? "" : "dis");

		for (link = sources; link; link = g_list_next (link)) {
			source = link->data;

			if (g_strcmp0 (e_source_get_parent (source), key) != 0)
				continue;

			printf ("\n");
			printf ("   Child source '%s'\n", e_source_get_display_name (source));
			printf ("      Enabled: %s\n", e_source_get_enabled (source) ? "true" : "false");
			printf ("      Type: %s\n",
				e_source_has_extension (source, E_SOURCE_EXTENSION_CALENDAR) ? "Calendar" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_TASK_LIST) ? "Task List" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_MEMO_LIST) ? "Memo List" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_ADDRESS_BOOK) ? "Address Book" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_MAIL_ACCOUNT) ? "Mail Account" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_MAIL_IDENTITY) ? "Mail Itentity" :
				e_source_has_extension (source, E_SOURCE_EXTENSION_MAIL_SUBMISSION) ? "Mail Submission" :
				"Unchecked/Unknown");
		}

		printf ("\n");
	}

	g_list_free_full (sources, g_object_unref);
	g_hash_table_destroy (goas);

	g_object_unref (registry);

	return 0;
}