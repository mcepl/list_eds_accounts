#!/usr/bin/env python

from __future__ import print_function

import gi
gi.require_version('EDataServer', '1.2')
from gi.repository import EDataServer

SOURCE_TYPES = (
    EDataServer.SOURCE_EXTENSION_CALENDAR,
    EDataServer.SOURCE_EXTENSION_TASK_LIST,
    EDataServer.SOURCE_EXTENSION_MEMO_LIST,
    EDataServer.SOURCE_EXTENSION_ADDRESS_BOOK,
    EDataServer.SOURCE_EXTENSION_MAIL_ACCOUNT,
    EDataServer.SOURCE_EXTENSION_MAIL_IDENTITY,
    EDataServer.SOURCE_EXTENSION_MAIL_SUBMISSION,
)
DISPLAYED_SOURCE_TYPES = ("Calendar", "Task List", "Memo List", "Address Book",
                          "Mail Account")

# check for exceptions???
src_reg = EDataServer.SourceRegistry.new_sync()
if src_reg is None:
    raise RuntimeError('Registry is NULL, but no error given')

goa_accs = [x for x in src_reg.list_enabled()
            if x.has_extension(EDataServer.SOURCE_EXTENSION_GOA)]

print("Found %d GOA enabled accounts" % len(goa_accs))

for goa_acc in goa_accs:
    collection = goa_acc.get_extension(EDataServer.SOURCE_EXTENSION_COLLECTION)

    print('Account {}'.format(goa_acc.get_display_name()))
    print('\tBackend: {}'.format(collection.get_backend_name()))
    print('\tEnabled: {}'.format(goa_acc.get_enabled()))
    print('\tCalendar: {}'.format(collection.get_calendar_enabled()))
    print('\tContacts: {}'.format(collection.get_contacts_enabled()))
    print('\tMail: {}'.format(collection.get_mail_enabled()))

    for in_acc in src_reg.list_sources():
        if in_acc.get_parent() == goa_acc.get_uid():
            disp_exts = [x for x in SOURCE_TYPES if in_acc.has_extension(x)]
            disp_ext = disp_exts[0] if disp_exts else 'Unchecked/Unknown'
            back_ext = in_acc.get_extension(disp_ext)
            if disp_ext in DISPLAYED_SOURCE_TYPES:
                print('\tChild source: {}'.format(in_acc.get_display_name()))
                print('\t\tBackend: {}'.format(back_ext.get_backend_name()))
                print('\t\tEnabled: {}'.format(in_acc.get_enabled()))
                print('\t\tType: {}'.format(disp_ext))
    print()
